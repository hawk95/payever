const superset = require( 'supertest' );
const should = require( 'should' );

const server = superset.agent( 'http://localhost:3000' );

describe( 'Unit tests for users', function () {
    it( 'should fetch a single user in json format', function ( done ) {
        server.get( '/api/users/1' )
            .expect( "Content-type", /json/ )
            .expect( 200 )
            .end( function ( err, res ) {
                if (err) throw err;
                should(res.body.id).equal(1);
                done();
            } )
    } );

    it( 'should return user avatar in base64', function ( done ) {
        server.get( '/api/users/1/avatar' )
            .expect( "Content-type", /json/ )
            .expect( 200 )
            .end( function ( err, res ) {
                if (err) throw err;
                should(res.body.data).startWith('data:image/jpeg;base64,');
                done();
            } )
    } );

    it( 'should delete user avatar', function ( done ) {
        server.delete( '/api/users/1/avatar' )
            .expect( "Content-type", /json/ )
            .expect( 200 )
            .end( function ( err, res ) {
                if (err) throw err;
                should(res.body).value('data', `avatar-1.txt`);
                done();
            } )
    } );
} );