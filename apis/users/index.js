import express from 'express';
import request from 'request';
import fs from 'fs';
import path from 'path';

import { cronRunner } from '../../utils';
import config from './config';

const router = express.Router();
const appDir = path.dirname( require.main.filename );

let page = 1;

if ( process.env.SCRAP ) {
    cronRunner( 60000, () => {
        request( {
            url: `${config.url}/?page=${page}`,
            method: 'GET',
            json: true
        }, ( err, response, body ) => {
            const { data } = body;

            if ( err || !data.length ) return;

            fs.appendFile( path.resolve( appDir, 'user-list.txt' ), JSON.stringify( ...data ), ( err ) => {
                if ( err ) {
                    console.log( 'Failed to append users to the list' );
                    return;
                }

                console.log( `Page ${page}` );

                page++;
            } );
        } )
    } );
}

router.get( '/:userId', ( req, res ) => {
    const { userId } = req.params;

    request( {
        url: `${config.url}/${userId}`,
        method: 'GET',
        json: true
    }, ( err, response, body ) => {
        res.send( body.data )
    } )
} );


router.get( '/:userId/avatar', ( req, res ) => {
    const { userId } = req.params;
    const avatarFilePath = path.resolve( appDir, `./avatars/avatar-${userId}.txt` );
    const fileName = `avatar-${userId}.txt`;

    if ( fs.existsSync( avatarFilePath ) ) {
        fs.readFile( avatarFilePath, 'utf8', (err, data) => {
            if ( err ) {
                res.send({
                    status: 'Failed to get file!',
                    error: err
                });

                return;
            }

            res.status(200).send({
              data
            });
        } );

        // res.set( 'Content-Disposition', `attachment;filename=${fileName}` );
        // res.sendFile( path.resolve( appDir, 'avatars', fileName ) );

        return;
    }

    request( {
        url: `${config.url}/${userId}`,
        method: 'GET',
        json: true
    }, ( err, response, body ) => {
        const { data } = body;
        const avatarURL = data.avatar;

        request( { encoding: 'binary', url: avatarURL, method: 'GET' }, ( err, response, body ) => {
            if ( !err ) {
                const avatar = fs.createWriteStream( avatarFilePath );
                const imagePrefix = 'data:' + response.headers[ 'content-type' ] + ';base64,';
                const image = new Buffer( body.toString(), 'binary' ).toString( 'base64' );

                avatar.write( imagePrefix + image, () => {
                    res.send( { data: imagePrefix + image } )
                } );
            }
        } )
    } )
} );

router.delete( '/:userId/avatar', ( req, res ) => {
    const { userId } = req.params;
    const fileName = `avatar-${userId}.txt`;

    fs.unlink( path.resolve( appDir, 'avatars', fileName ), err => {
        if ( err ) {
            res.send( {
                status: 'Failed to delete file!',
                error: err
            } );
        }

        res.send( {
            status: 'Successfully file deleted!',
            data: fileName
        } )
    } )
} );

export default router;