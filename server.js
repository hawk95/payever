import express from 'express';
import bodyParser from 'body-parser';
import apis from './apis';

// Configuration constants
const HOSTNAME = process.env.HTTP_INTERFACE || 'localhost';
const PORT = process.env.PORT || 3000;

const app = express();

app.use(bodyParser.json());

app.use('/api', apis);

// Error handling
app.use('*', (req, res) => {
  res.status(404).send({
    message: 'Page not found'
  })
});

app.listen(PORT, HOSTNAME, () => {
  console.log(`Server started at: http://${HOSTNAME}:${PORT}`);
});