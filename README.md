####Commands to run
`npm run start` - will start the application in watch mode and ready for any changes

`npm run cron` - the same as `start` but will also start a cron job to fetch users

`npm run test` - will test the 3 endpoints available, before running the tests the application must be started