export const cronRunner = ( interval, fn ) => {
    function job(){
        clearTimeout( timeout );
        timeout = setTimeout( job, interval );
        fn()
    }

    let timeout = setTimeout(job, interval );
};